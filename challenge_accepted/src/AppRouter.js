'use strict';

import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import MainComponent from './components/main/MainComponent';

const AppRouter = () => {
  return (
    <Router>

      {/* side-menu wrapper covering the entire app without the authentication */}
      {/*<Scene key="drawer" component={SideMenu} open={false} hideNavBar={true}>*/}
        <Scene key='drawerChildrenWrapper' hideNavBar={true}>

          <Scene key="mainComponent" component={MainComponent} animation="fade" duration={500} initial />

        </Scene>
      {/*</Scene>*/}

    </Router>
  );
};

export default AppRouter;
