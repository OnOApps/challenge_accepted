'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, LayoutAnimation } from 'react-native';
import { Actions } from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable';
import * as actions from '../../actions';

class MainComponent extends Component {

  componentWillMount(){
    this.props.dummyAction();
  }

  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut();
  }

  render() {
    return (
      <View style={styles.container}>
        <Animatable.Text animation="shake">MainComponent</Animatable.Text>
        <Text>{this.props.dummyData}</Text>
      </View>
    );
  }

}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'

  }
};

const mapStateToProps = state => {
  const { dummyData } = state.dummy;

  return { dummyData };
};

export default connect(mapStateToProps, actions)(MainComponent);
