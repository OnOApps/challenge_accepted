'use strict';
import * as types from './types';

export const dummyAction = () => {
  let dummyContent = "Dummy Content";

  return (dispatch) => {

    setTimeout(() => {
      dispatch({
        type: types.dummy,
        payload: dummyContent
      });
    },3000);
  };
};