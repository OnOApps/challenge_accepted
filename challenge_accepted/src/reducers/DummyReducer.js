import * as types from '../actions/types';

const INITIAL_STATE = {
  dummyData: "",
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case types.dummy:
      return { ...state, dummyData: action.payload};
    default:
      return state;
  }
}