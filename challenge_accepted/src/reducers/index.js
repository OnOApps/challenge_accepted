import { combineReducers } from  'redux';
import DummyReducer from './DummyReducer';

//this is the apps store state as received from the different reducers
export default combineReducers({
  dummy: DummyReducer,
});