import {AppRegistry} from 'react-native';
import App from './src/App';

AppRegistry.registerComponent('challenge_accepted', () => App);